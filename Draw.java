
public class Draw {

    public static double derivada;
    public static double function;
    
    public static void draw(Plano p, String functionString){
        function = 0;
        derivada = 0;
        for (double x = -10; x < 10; x += 0.001f) {
            if (functionString.contains("x") && !functionString.contains("*") && !functionString.contains("\\/") && 
                !functionString.contains("^") && !functionString.contains("+") && !functionString.contains("sin") && !functionString.contains("sen")
                && !functionString.contains("cos")) {
                simpleFunction(x, functionString);
            } else if(functionString.contains("sin") || functionString.contains("sen")) {
                String[] funtionArray = functionString.split("\\("); 
                String[] funtionArray2 = funtionArray[1].split("\\)"); 
                if (funtionArray2[0].length() > 1) {
                    String[] number = funtionArray2[0].split("x");
                    function = Integer.parseInt(number[0]) * Math.sin(x);
                    derivada = Integer.parseInt(number[0]) * Math.cos(x);
                } else {
                    function = Math.sin(x);
                    derivada = Math.cos(x);
                }
            }
            else if(functionString.contains("cos") ) {
                String[] funtionArray = functionString.split("\\("); 
                String[] funtionArray2 = funtionArray[1].split("\\)"); 
                if (funtionArray2[0].length() > 1) {
                    String[] number = funtionArray2[0].split("x");
                    function = Integer.parseInt(number[0]) * Math.cos(x);
                    derivada = Integer.parseInt(number[0]) * (-Math.sin(x));
                } else {
                    function = Math.cos(x);
                    derivada = -Math.sin(x);
                }
            } 
            else if (functionString.contains("^")) {
                String[] funtionArray = functionString.split("\\^");
                if (funtionArray[0].length() > 1) {
                    String[] number = funtionArray[0].split("x");
                    function = Integer.parseInt(number[0]) * Math.pow(x, Integer.parseInt(funtionArray[1]));
                    derivada = Integer.parseInt(funtionArray[1]) * Integer.parseInt(number[0]) * Math.pow(x, Integer.parseInt(funtionArray[1])-1);
                } else {
                    function = Math.pow(x, Integer.parseInt(funtionArray[1]));
                    derivada = Integer.parseInt(funtionArray[1]) * Math.pow(x, Integer.parseInt(funtionArray[1]) - 1);
                }
            } else if (functionString.contains("+")) {
                String[] funtionArray = functionString.split("\\+");
                if (funtionArray[0].length() > 1) {
                    if (funtionArray[1].length() > 1) {
                        String[] number = funtionArray[1].split("x");
                        function = Integer.parseInt(number[0]) * x;
                        derivada = Integer.parseInt(number[0]);
                    } else {
                        function = x;
                        derivada = 1;
                    }
                    String[] number = funtionArray[0].split("x");
                    function += Integer.parseInt(number[0]) * x;
                    derivada += Integer.parseInt(number[0]);
                } else {
                    function = 2 * x;
                    derivada = 2;
                }
            }
            else if (functionString.contains("*")) {
                String[] functionArray = functionString.split("\\*");
                if (functionArray[0].contains("x") && functionArray[1].contains("x")) {
                    if (functionString.length() > 3) {
                        String[] u = functionArray[0].split("x");
                        String[] v = functionArray[1].split("x");
                        function = Integer.parseInt(u[0]) * x * Integer.parseInt(v[0]) * x ;
                        derivada = Integer.parseInt(u[0]) * Integer.parseInt(v[0]) * x + Integer.parseInt(u[0]) * x * Integer.parseInt(v[0]);
                    } else {
                        function = x * x;
                        derivada = 1 * x + x * 1;
                    }
                } else if(functionArray[0].contains("x") && !functionArray[1].contains("x")) {
                    String[] u = functionArray[0].split("x");
                    function = Integer.parseInt(u[0]) * x * Integer.parseInt(functionArray[1]);
                    derivada = Integer.parseInt(u[0]) * Integer.parseInt(functionArray[1]);
                } else if (!functionArray[0].contains("x") && functionArray[1].contains("x"))  {
                    String[] v = functionArray[1].split("x");
                    function = Integer.parseInt(functionArray[0]) * Integer.parseInt(v[0]) * x;
                    derivada = Integer.parseInt(functionArray[0]) * Integer.parseInt(v[0]);
                }
            } 
            double y = (double) function;
            p.dibujarPunto(x, y);   
            y = (double) derivada;
            p.dibujarPuntoDerivada(x, y);
        }
    }

    public static void simpleFunction(double x, String functionString) {
        if (functionString.length() > 1) {
            String[] number = functionString.split("x");
            function = Integer.parseInt(number[0]) * x;
            derivada = Integer.parseInt(number[0]);
        } else {
            function = x;
            derivada = 1;
        }
    }
}
