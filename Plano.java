import java.awt.Graphics2D;
import javax.swing.JPanel;
import java.awt.Color;

public class Plano {
    private JPanel panel;
    private int width;
    private int height;
    private int scale;
    public Plano(JPanel panel, int scale) {
        this.scale = scale;
        this.panel = panel;
        width = panel.getWidth();
        height = panel.getHeight();
       }
    
    public void dibujar() {        
        Graphics2D grafic = (Graphics2D) panel.getGraphics();        
        grafic.clearRect(0, 0, width, height);
        int x2 = width/2;
        for (int x = width/2; x < width; x += scale){
            grafic.drawLine(x, 0, x, height);
            grafic.drawLine(x2, 0, x2, height);
            x2 = x2 - scale;
        }
        int y2 = height/2;
        for (int y = height/2; y < height; y += scale){
            grafic.drawLine(0, y, width, y);
            grafic.drawLine(0, y2, width, y2);
            y2 = y2 - scale;
        }
        grafic.setColor(Color.red);
        grafic.drawLine(width/2, 0, width/2, height);
        grafic.drawLine(0, height/2, width, height/2);          

    }
    
    public void dibujarPunto(double x, double y){
        Graphics2D g = (Graphics2D) panel.getGraphics(); 
        int xDigital = (int) (width/2 + x*scale);
        int yDigital = (int) (height/2 - y*scale);
        g.fillOval(xDigital - 3, yDigital - 3, 6, 6);
    }

    public void dibujarPuntoDerivada(double x, double y){
        Graphics2D g = (Graphics2D) panel.getGraphics(); 
        g.setColor(Color.blue);
        int xDigital = (int) (width/2 + x*scale);
        int yDigital = (int) (height/2 - y*scale);
        g.fillOval(xDigital - 3, yDigital - 3, 6, 6);
    }
}
